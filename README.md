[![Release](https://jitpack.io/v/org.traffxml/traff-libalertclocation.svg)](https://jitpack.io/#org.traffxml/traff-libalertclocation)

# Creating a location table database

To create a location table database, first build the jar with Maven. Then run:

    java -cp /path/to/libalertclocation.jar:/path/to/hsqldb.jar org.traffxml.lib.alertclocation.util.LclTool -ltdb "/path/to/ltdb" -lt /path/to/ltef

Where:

* `/path/to/libalertclocation.jar` is the JAR file for traff-libalertclocation
* `/path/to/hsqldb.jar` is the path to the current HSQLDB JAR
* `/path/to/ltdb` is the database to which the data will be added, with its full path name
* `/path/to/ltef` is the path to the folder which holds the location tables in LTEF format, which can be one of the following:
  * A folder holding a single set of `.DAT` files
  * A folder holding any number of subfolders, each holding a single set of `.DAT` files

# Javadoc

Javadoc for `master` is at https://traffxml.gitlab.io/traff-libalertclocation/javadoc/master/.

Javadoc for `dev` is at https://traffxml.gitlab.io/traff-libalertclocation/javadoc/dev/.
