package org.traffxml.lib.alertclocation;

public class SupplementaryInfo {
	public final int code;
	public final String text;

	SupplementaryInfo(String line) {
		String[] comp = AlertC.colonPattern.split(line);
		this.code = Integer.parseInt(comp[1]);
		this.text = comp[2];
	}
	
	@Override
	public String toString() {
		return "[" + this.code + "] " + this.text;
	}
}
