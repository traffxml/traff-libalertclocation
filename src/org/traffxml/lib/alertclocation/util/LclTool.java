/*
 * Copyright © 2015–2019 traffxml.org.
 * 
 * This file is part of the traff-libalertclocation library.
 *
 * The library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the library.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.traffxml.lib.alertclocation.util;

import java.io.File;
import org.traffxml.lib.alertclocation.AlertC;

public class LclTool {
	private static String getParam(String param, String[] args, int pos) {
		if(pos >= args.length) {
			System.out.println("-" + param + " needs a filename.");
			System.exit(0);
		}
		return args[pos];
	}
	
	public static void main(String[] args) {
		System.out.println("LCL Tool - (C) traffxml.org, 2015-2019.");
		
		String inLtPath = null;
		String dbUrl = "jdbc:hsqldb:mem:.";
		
		for(int i = 0; i < args.length; i++) {
			if("-lt".equals(args[i])) {
				inLtPath = getParam("lt", args, ++i);
			} else if("-ltcharset".equals(args[i])) {
				AlertC.setCharset(getParam("ltcharset", args, ++i));
			} else if("-ltdb".equals(args[i])) {
				dbUrl = String.format("jdbc:hsqldb:file:%s", getParam("ltdb", args, ++i));
			} else {
				System.out.println("Unknown argument: " + args[i]);

				System.out.println("Arguments:");
				System.out.println("  -lt <path>               Read AlertC location tables found at the given path (or subdirs)");
				System.out.println("  -ltcharset <charset>     Use a specific charset for reading AlertC location tables");
				System.out.println("  -ltdb <path>             Use AlertC location database at the given path");
				System.exit(1);
			}
		}

		if (inLtPath == null) {
			System.out.println("A set of AlertC location tables must be provided. Aborting.");
			System.exit(0);
		}
		
		AlertC.setDbUrl(dbUrl);
		
		// Build db if needed
		if (inLtPath != null) {
			System.out.println("Processing AlertC location tables...");
			AlertC.readLocationTables(new File(inLtPath));
			System.out.println("Done processing AlertC location tables.");
			System.exit(0);
		}
	}
}
