package org.traffxml.lib.alertclocation;

import java.sql.ResultSet;
import java.sql.SQLException;

/** Describes an AlertC location name. */
public class AlertCName {
	/** The country ID used in AlertC messages. */
	public int cid;
	/** The language ID for the name. */
	public int lid;
	/** The name ID by which locations refer to the name. */
	public int nid;
	/** The name. */
	public String name;
	public String nameComment;
	
	/**
	 * Creates a new {@code AlertCName} from a given record.
	 * 
	 * <p>This constructor expects one argument, {@code rset}, which must be a result set obtained by
	 * querying the {@code Names} table. Prior to calling the constructor, the cursor for
	 * {@code rset} must be set. The constructor will use the data from the record which the cursor
	 * points to.
	 * 
	 * @param rset The result set
	 * @throws SQLException
	 */
	AlertCName(ResultSet rset) throws SQLException {
		this.cid = rset.getInt("CID");
		this.lid = rset.getInt("LID");
		this.nid = rset.getInt("NID");
		this.name = rset.getString("NAME");
		this.nameComment = rset.getString("NCOMMENT");
		if (rset.wasNull())
			this.nameComment = "";
	}
}
